import network
import socket
import utime
import struct

from machine import Pin, PWM, RTC
import uasyncio
import ntptime

tzOffset = 3600
ntpHost = 'de.pool.ntp.org'

# WiFi credentials
wifiSSID = 'NetzwerkName'
wifiPwd = 'Passwort'

# configure PWM Pins, I mixed up red and green...
rgbw = [PWM(Pin(2)), PWM(Pin(1)), PWM(Pin(3)), PWM(Pin(0))]

# set all PWM frequencies to 1 kHz (number of times the PWM counter wraps per second)
for i in range(4):
    rgbw[i].freq(1000)

# connect to WiFi
wifi = network.WLAN(network.STA_IF)

# HTML source, with some placeholders which are filled with dynamic values when sent to a requester
html = """<!DOCTYPE html>
<html>
<head><title>Pico Tageslichtwecker</title></head>
<body>
<p>
<form action=\"\" method=\"post\">
<label>Startfarbe <input type=\"color\" name=\"start\" value=\"#%s\"></label>
<label>Endfarbe <input type=\"color\" name=\"end\" value=\"#%s\"></label>
<label>Helligkeit <input type=\"range\" name=\"intensity\" min=\"1\" max=\"255\" value=\"%d\"></label>
<label>Fading-Dauer <input type=\"time\" name=\"tfade\" value=\"%02d:%02d\"></label>
<label>Startzeit <input type=\"time\" name=\"tstart\" value=\"%02d:%02d\"></label>

<button type=\"submit\" name=\"action\" value=\"colour\">Setzen</button>
</form>
</p>

<p>
<form action=\"\" method=\"post\">
<label>Helligkeit <input type=\"range\" name=\"wlintensity\" min=\"1\" max=\"255\" value=\"%d\"></label>
<button type=\"submit\" name=\"action\" value=\"whitelight\">%s</button>
</form>
</p>

<p><form action=\"\" method=\"post\">
<button type=\"submit\" name=\"action\" value=\"settime\">Zeit einstellen (NTP)</button>
</form></p>

<p>Time: %02d.%02d.%04d %02d:%02d:%02d</p>

<p>Last action: %s</p>
</body>
</html>
"""

# functions start here
def wifiStart():
    wifi.active(True)
    wifi.connect(wifiSSID, wifiPwd)
    
    # wait until WiFi is connected
    retries = 10
    
    # we have a timeout of 10 seconds here during which we're trying to connect.
    while retries > 0:
        # abort in case of errors
        if wifi.status() < 0 or wifi.status() >= 3:
            break
        retries -= 1
        print('not connected\t' + str(retries))
        utime.sleep(1)

    if wifi.status() != 3:
        raise RuntimeError('network connection failed')
    else:
        print('connected, will now wait for HTTP requests')
        status = wifi.ifconfig()
        print( 'My IP: ' + status[0])

async def handleRequest(reader, writer):
    # white LED status and corresponding button text
    global whiteLightButton, whiteLightOn, wli
    global rgbiStart, rgbiEnd, rgbFadingTime, alarmSet, alarmHour, alarmMinute
    
    rgbFadingTime = 10

    print("new request")
    # read all data from the stream reader
    request_data = await reader.read(1024)
    request = str(request_data)
    print(request)
    # see whether this shall trigger some action
    actionOff = request.find('action')
    actionId = "None"
        
    lt = utime.localtime()
    
    if actionOff > 0:
        # extract action identifier
        actionId = request[actionOff + 7:-1]
        
        if actionId == "whitelight":
            if whiteLightOn:
                rgbw[3].duty_u16(0)
                whiteLightButton = "Ein"
                whiteLightOn = False
            else:
                wliOff = request.find("wlintensity")
                
                if wliOff > 0:
                    wliEnd = request.find("&", wliOff)
                    print(str(wliOff) + " " + str(wliEnd) + ": " + request[wliOff + 12:wliEnd])
                    wli = int(request[wliOff + 12:wliEnd])
                
                rgbw[3].duty_u16(wli * 256)
                whiteLightButton = "Aus"
                whiteLightOn = True
        elif actionId == "colour":
            intensityOff = request.find('intensity')
            # extract colour values
            scol = request[intensityOff - 21:intensityOff - 15]
            ecol = request[intensityOff - 7:intensityOff - 1]
            rgbIntensity = int(request[intensityOff + 10:actionOff - 30])
            # extract fading time
            rgbFadingTime = int(request[actionOff - 18:actionOff - 16]) + int(request[actionOff - 23:actionOff - 21]) * 60
            alarmHour = int(request[actionOff - 8:actionOff - 6])
            alarmMinute = int(request[actionOff - 3:actionOff - 1])
            
            # turn off the lights
            rgbw[0].duty_u16(0)
            rgbw[1].duty_u16(0)
            rgbw[2].duty_u16(0)
            
            rgbiStart = [int(scol[0:2], 16), int(scol[2:4], 16), int(scol[4:], 16), 1]
            rgbiEnd = [int(ecol[0:2], 16), int(ecol[2:4], 16), int(ecol[4:], 16), rgbIntensity]
            
            print("start colour " + scol + " end colour " + ecol + " intensity " + str(rgbiEnd[3]) + " fading time " + str(rgbFadingTime))
            print("start colour RGB: " + str(rgbiStart[0]) + " " + str(rgbiStart[1]) + " " + str(rgbiStart[2]))
            print("end colour RGB: " + str(rgbiEnd[0]) + " " + str(rgbiEnd[1]) + " " + str(rgbiEnd[2]))
            
            configureColourFade()
            alarmSet = True
        elif actionId == "settime":
            ntptime.settime()
        
    rgbs = "%02x%02x%02x" % (rgbiStart[0], rgbiStart[1], rgbiStart[2])
    rgbe = "%02x%02x%02x" % (rgbiEnd[0], rgbiEnd[1], rgbiEnd[2])
    rgbfm = int(rgbFadingTime / 60)
    rgbfs = rgbFadingTime - rgbfm * 60
    
    response = html % (rgbs, rgbe, rgbiEnd[3], rgbfm, rgbfs, lt[3], lt[4], wli, whiteLightButton, lt[2], lt[1], lt[0], lt[3], lt[4], lt[5], actionId)
    
    writer.write('HTTP/1.0 200 OK\r\nContent-type: text/html\r\n\r\n')
    writer.write(response)
    
    # wait until all data has been transmitted
    await writer.drain()
    await writer.wait_closed()

def configureColourFade():
    global rgbiStart, rgbiEnd, rgbFadingTime, rgbFadingCounter, rgbValues, rgbDelta
    
    fadingTime = rgbFadingTime * 10
    rgbFadingCounter = fadingTime
    
    rgbDelta = [0, 0, 0]
    rgbValues = [0, 0, 0]
    
    rgbDelta[0] = int(((rgbiEnd[0] * rgbiEnd[3] - rgbiStart[0] * rgbiStart[3]) << 16) / fadingTime)
    rgbDelta[1] = int(((rgbiEnd[1] * rgbiEnd[3] - rgbiStart[1] * rgbiStart[3]) << 16) / fadingTime)
    rgbDelta[2] = int(((rgbiEnd[2] * rgbiEnd[3] - rgbiStart[2] * rgbiStart[3]) << 16) / fadingTime)
    rgbValues[0] = rgbiStart[0] * rgbiStart[3] << 16
    rgbValues[1] = rgbiStart[1] * rgbiStart[3] << 16
    rgbValues[2] = rgbiStart[2] * rgbiStart[3] << 16
    
    print("RGB values " + str(rgbValues[0]) + " " + str(rgbValues[1]) + " " + str(rgbValues[2]))
    print("RGB delta " + str(rgbDelta[0]) + " " + str(rgbDelta[1]) + " " + str(rgbDelta[2]))

async def doColourFade():
    global rgbFadingCounter, rgbValues, rgbDelta
    
    while rgbFadingCounter > 0:
        rgbw[0].duty_u16(rgbValues[0] >> 16)
        rgbw[1].duty_u16(rgbValues[1] >> 16)
        rgbw[2].duty_u16(rgbValues[2] >> 16)
        rgbValues[0] += rgbDelta[0]
        rgbValues[1] += rgbDelta[1]
        rgbValues[2] += rgbDelta[2]
        
        rgbFadingCounter -= 1
        
        await uasyncio.sleep(0.1)

async def setTimeNtp():
    epochDelta = 2208988800
    ntpRequest = bytearray(48)
    ntpRequest[0] = 0x1b
    # resolve host domain, NTP uses port 123
    addr = socket.getaddrinfo(ntpHost, 123)[0][-1]
    # open a socket which connects to the NTP host. NTP uses UDP (datagram).
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    
    try:
        s.settimeout(1)
        res = s.sendto(ntpRequest, addr)
        # NTP packets are always 48 bytes
        msg = s.recv(48)
    finally:
        # close the socket
        s.close()
    
    ntpTime = struct.unpack("!I", msg[40:44])[0]
    #print("ntpTime " + str(ntpTime))
    t = utime.gmtime(ntpTime - epochDelta + tzOffset)
    # check day of week - in struct_time 0 is Monday, in RP2040 RTC 0 is Sunday
    machine.RTC().datetime(t[0], t[1], t[2], t[6] + 1, t[3], t[4], t[5], 0)

async def main():
    global whiteLightButton, whiteLightOn, wli
    global rgbiStart, rgbiEnd, rgbFadingTime, alarmSet, alarmHour, alarmMinute
    
    whiteLightButton = "Ein"
    whiteLightOn = False
    wli = 127
    
    rgbiStart = [0, 0, 0, 1]
    rgbiEnd = [255, 0, 0, 127]
    rgbFadingTime = 10
    rgbIntensity = 127
    
    alarmSet = False
    
    print(utime.localtime())
    
    wifiStart()
    
    # this starts a TCP/IP server, hiding all the sockets stuff
    uasyncio.create_task(uasyncio.start_server(handleRequest, "0.0.0.0", 80))
    
    while True:
        if alarmSet:
            lt = utime.localtime()
            
            if lt[3] == alarmHour and lt[4] == alarmMinute:
                await doColourFade()
                alarmSet = False
        
        await uasyncio.sleep(1)

try:
    uasyncio.run(main())
finally:
    uasyncio.new_event_loop()