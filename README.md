# rppico-upython

Contains some MicroPython projects for Raspberry Pi Pico (W).

daylight_alarm.py is an alarm clock intended to simulate sunrise. It runs on the Pico W and is configured via your web browser.

pico-asteroids.py is an Asteroids clone developed for the Picoboy (www.picoboy.de). It can be used with other RP2040 boards as well as long as you add the same components (STK8BA58 motion sensor, five way joystick and a 128x64 OLED display).